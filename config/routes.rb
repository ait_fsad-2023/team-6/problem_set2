Rails.application.routes.draw do
  resources :quotations do
    member do
      delete 'destroy'  # Use DELETE for destroy action
      get 'edit'
      patch 'update'
    end
  end
 
  get 'problemset1', to: 'problemset1#subpage'
  get 'q1', to: 'q1#show'
  get 'q3', to: 'news#index'

  get 'q2', to: 'q2#index'
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html
  root 'main_site#index'

  get 'problemset2', to: 'problemset2#subpage'
  get 'quotations', to: 'quotations#quotations'

  
  # Defines the root path route ("/")
  # root "articles#index"
end
