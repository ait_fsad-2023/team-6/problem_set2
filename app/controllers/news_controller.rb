class NewsController < ApplicationController
    require 'nokogiri'
    require 'httparty'
  
    def index
      @nyt_headlines = fetch_nyt_headlines
      @bangkok_post_headlines = fetch_bangkok_post_headlines
    end
  
    private
  
    def fetch_nyt_headlines
      nyt_url = 'https://www.nytimes.com'
      response = HTTParty.get(nyt_url)
      doc = Nokogiri::HTML(response.body)
  
      # Find all links containing "/world/" in the URL
      links = doc.css('a').select { |link| link['href']&.include?('/world/') }
  
      headlines = links.map do |link|
        {
          text: link.text,
          href: URI.join(nyt_url, link['href']).to_s
        }
      end
  
      headlines
    end
  
    def fetch_bangkok_post_headlines
      bangkok_post_url = 'https://www.bangkokpost.com'
      response = HTTParty.get(bangkok_post_url)
      doc = Nokogiri::HTML(response.body)
  
      # Find all links containing "/thailand/" in the URL
      links = doc.css('a').select { |link| link['href']&.include?('/thailand/') }
  
      headlines = links.map do |link|
        {
          text: link.text,
          href: URI.join(bangkok_post_url, link['href']).to_s
        }
      end
  
      headlines
    end
  end

  