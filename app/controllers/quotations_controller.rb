class QuotationsController < ApplicationController
  def quotations
    if params[:quotation]
      @quotation = Quotation.new(params[:quotation])
      if @quotation.save
        flash[:notice] = 'Quotation was successfully created.'
        @quotation = Quotation.new
      end
    else
      @quotation = Quotation.new
    end
  
    if params[:sort_by] == "date"
      @quotations = Quotation.order(:created_at)
    else
      @quotations = Quotation.order(:category)
    end
  end

  def index
    if params[:query].present?
      @quotations = Quotation.where("LOWER(quote) LIKE ? OR LOWER(author_name) LIKE ?", "%#{params[:query].downcase}%", "%#{params[:query].downcase}%")
    else
      @quotations = Quotation.all
      respond_to do |format|
        format.html
        format.xml { render xml: @quotations }
        format.json { render json: @quotations }
      end
    end
  end

  def new
    @quotation = Quotation.new
    @existing_categories = Quotation.distinct.pluck(:category)
  end

  def create
    @quotation = Quotation.new(quotation_params)

    if @quotation.save
      flash[:notice] = 'Quotation was successfully created.'
      redirect_to quotations_path
    else
      @existing_categories = Quotation.distinct.pluck(:category)
      render :new
    end
  end

  def kill_quotation
    # Assume params[:id] contains the quotation ID to be killed
    killed_quotations = cookies[:killed_quotations].to_s.split(' ')
    killed_quotations << params[:id]
    cookies[:killed_quotations] = killed_quotations.uniq.join(' ')
  end
  
  # In your controller, erase personalization
  
  def erase_personalization
    cookies.delete(:killed_quotations)
  end

  def destroy
    @quotation = Quotation.find(params[:id])
    @quotation.destroy
    redirect_to quotations_path, notice: 'Quotation was successfully deleted.'
  end

  def edit
    @quotation = Quotation.find(params[:id])
  end

  def update
    @quotation = Quotation.find(params[:id])

    if @quotation.update(quotation_params)
      redirect_to quotations_path, notice: 'Quotation was successfully updated.'
    else
      render :edit
    end
  end

  private

  def quotation_params
    params.require(:quotation).permit(:author_name, :category, :quote)
  end
  
    
end
